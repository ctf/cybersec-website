export const link = (url: string, name: string): string => {
    return `<u><a class="text-light-blue dark:text-dark-blue underline" href="${url}" target="_blank">${name}</a></u>`
}
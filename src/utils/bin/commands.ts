import * as bin from './index';
import { link } from './tools';
import config from '../../../config.json';

export const help = async (): Promise<string> => {
  const commands = Object.keys(bin).sort();

  let response = `Here is a list of all available commands:\n`

  for (let i = 1; i <= commands.length; i++) {
    response += `\n${commands[i - 1]}`;
  }

  response += `\n\n[tab]: trigger completion\n[ctrl+l]/clear: clear terminal\n`

  return response;
};

// About
export const about = async (args: string[]): Promise<string> => {
  return `Welcome to the University of Waterloo's Cybersecurity Club!\n
We are a community for anyone interested in learning more\nabout digital security, networking with their peers, and\nhoning their skills to prepare themselves for one of the\nfastest growing fields in IT.`;
};

// Contact
export const email = async (args: string[]): Promise<string> => {
  return link(`mailto:${config.email}`, config.email);
};

export const discord = async (args: string[]): Promise<string> => {
  window.open(`${config.social.discord}`);

  return 'Opening Discord...';
};

export const kickoff = async (args : string[]): Promise<string> => {
  return "To be Released...";
}

export const clubday = async (args: string[]): Promise<string> => {

  return 'Join UW Cybersecurity for Clubs and Societies Day!!\nWhere: SLC Great Hall\nWhen: Thursday, September 21st\n      Friday, September 22nd from 11 AM to 3 PM\nDrop by to learn about some exclusive opportunities!\nLooking forward to seeing you all there!';
};

export const ctf = async (args: string[]): Promise<string> => {

  return 'Our CTF working hours are\nevery Friday in MC2054 from 7:00 PM - 8:00 PM,\nwhere we compete in a different CTF every time!\nDrop by to chat with our execs,\nteam up with fellow club members,\nand hack away at some challenges! ';
};

export const partners = async (args: string[]): Promise<string> => {

  return 'We are actively looking for sponsors and partners.\nIf you are a corporation, startup, or student club\nlooking to collaborate with the UW Cybersecurity Club,\nplease email us at ' + link(`mailto:${config.email}`, config.email) + '.\nWe look forward to hearing from you! ';
};

export const gitlab = async (args: string[]): Promise<string> => {
  window.open(`${config.social.gitlab}`);

  return 'Opening GitLab...';
};

export const linkedin = async (args: string[]): Promise<string> => {
  window.open(`${config.social.linkedin}`);

  return 'Opening Linkedin...';
};

export const instagram = async (args: string[]): Promise<string> => {
  window.open(`${config.social.instagram}`);

  return 'Opening Instagram...';
};

// Search
export const google = async (args: string[]): Promise<string> => {
  window.open(`https://google.com/search?q=${args.join(' ')}`);
  return `Searching google for ${args.join(' ')}...`;
};

export const duckduckgo = async (args: string[]): Promise<string> => {
  window.open(`https://duckduckgo.com/?q=${args.join(' ')}`);
  return `Searching duckduckgo for ${args.join(' ')}...`;
};

export const bing = async (args: string[]): Promise<string> => {
  window.open(`https://bing.com/search?q=${args.join(' ')}`);
  return `Wow, really? You're gonna use Bing for ${args.join(' ')}?`;
};

export const reddit = async (args: string[]): Promise<string> => {
  window.open(`https://www.reddit.com/r/uwaterloo/`);
  return `Openning Reddit ...`;
};

// Typical linux commands
export const echo = async (args: string[]): Promise<string> => {
  return args.join(' ');
};

export const whoami = async (args: string[]): Promise<string> => {
  return `${config.ps1_username}`;
};

export const ls = async (args: string[]): Promise<string> => {
  return `nothing\nto\nsee\nhere\n:p`;
};

export const cd = async (args: string[]): Promise<string> => {
  return `unfortunately, we do not have the budget for more directories :(`;
};

export const date = async (args: string[]): Promise<string> => {
  return new Date().toString();
};

export const sudo = async (args?: string[]): Promise<string> => {
  window.open('https://www.youtube.com/watch?v=dQw4w9WgXcQ', '_blank');
  return `Permission denied: With little power comes... no responsibility? <3`;
};

// Banner
export const banner = (args?: string[]): string => {
  if (window.innerWidth <= 760) {
    return `
University of Waterloo
Cybersecurity Club
         
Type 'help' to see the list of available commands.
`;
  }
  return `
  ██╗   ██╗██╗    ██╗     ██████╗██╗   ██╗██████╗ ███████╗██████╗ ███████╗███████╗ ██████╗██╗   ██╗██████╗ ██╗████████╗██╗   ██╗
  ██║   ██║██║    ██║    ██╔════╝╚██╗ ██╔╝██╔══██╗██╔════╝██╔══██╗██╔════╝██╔════╝██╔════╝██║   ██║██╔══██╗██║╚══██╔══╝╚██╗ ██╔╝
  ██║   ██║██║ █╗ ██║    ██║      ╚████╔╝ ██████╔╝█████╗  ██████╔╝███████╗█████╗  ██║     ██║   ██║██████╔╝██║   ██║    ╚████╔╝ 
  ██║   ██║██║███╗██║    ██║       ╚██╔╝  ██╔══██╗██╔══╝  ██╔══██╗╚════██║██╔══╝  ██║     ██║   ██║██╔══██╗██║   ██║     ╚██╔╝  
  ╚██████╔╝╚███╔███╔╝    ╚██████╗   ██║   ██████╔╝███████╗██║  ██║███████║███████╗╚██████╗╚██████╔╝██║  ██║██║   ██║      ██║   
   ╚═════╝  ╚══╝╚══╝      ╚═════╝   ╚═╝   ╚═════╝ ╚══════╝╚═╝  ╚═╝╚══════╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚═╝   ╚═╝      ╚═╝   
         
Type 'help' to see the list of available commands.
`;
};


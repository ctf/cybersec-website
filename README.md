# UW Cybersecurity Club Website
Website for the University of Waterloo Cybersecurity Club

## Development
Requires the [yarn](https://classic.yarnpkg.com/lang/en/docs/install/#windows-stable) package manager

Install dependencies
```
yarn install
```

Run locally
```
yarn dev
```

## Docker
```
docker compose up -d
```

## Credit
Derived from Cveinnt's [LiveTerm](https://github.com/Cveinnt/LiveTerm) template
